<?php

namespace App\Controller;

use App\Entity\Adress;
use App\Entity\Order;
use App\Entity\User;
use App\Repository\AdressRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Exception\ValidationFailedException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;

class AuthController extends AbstractController
{
    public function __construct(private UserRepository $repo) {
    }

    #[Route('/api/user', methods: 'POST')]
    public function register(Request $request, SerializerInterface $serializer, UserPasswordHasherInterface $hasher): Response
    {
        $user = $serializer->deserialize($request->getContent(), User::class, 'json');
    
        //On vérifie s'il n'y a pas déjà un user avec cet email en base de données
        if($this->repo->findOneBy(['mail' => $user->getMail()])) {
            return $this->json(['error' => 'User already exists'], Response::HTTP_BAD_REQUEST);
        }
        //On hash son mot de passe (symfony rajoute automatiquement sel et probablement poivre)
        $hashedPassword = $hasher->hashPassword($user, $user->getPassword());
        $user->setPassword($hashedPassword);
        //On lui assigne un rôle par défaut
        $user->setRole('ROLE_USER');
       

        $order= new Order();
        $user->addOrder($order);
        $order->setStatus('cart');
        $order->setDetail('Merci pour votre commande. Voici le détail de celle-ci');
        $order->setTitle(('Commande'));
        $order->setTotal(0);

        $adress= new Adress();
        $user->addAdress($adress);
        $adress->addOrder($order);
        $adress->setStreetNum('');
        $adress->setStreet('test');
        $adress->setZipCode(0);
        $adress->setCity('');


        $this->repo->save($user, true);

        return $this->json($user);
    }

    #[Route('/api/account', methods: 'GET')]
    public function restrictedZone() {
        
        return $this->json($this->getUser());
    }

    #[Route("api/account/adress", methods: 'GET')]
    public function UserAdresses() {
        
        return $this->json($this->getUser()->getAdresses());
    }  
    
    #[Route('api/account/add', methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer, AdressRepository $adressRepository):Response
    {
        try {
            $adress=$serializer->deserialize($request->getContent(), Adress::class, 'json');
            $adress->setUser($this->getUser());
            $adressRepository->save($adress, true);

            return $this->json($adress, Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }

  

    #[Route("api/account/orders", methods: 'GET')]
    public function UserOrders() {
        
        return $this->json($this->getUser()->getOrders());
    }
}
