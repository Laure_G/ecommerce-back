<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class CategoryFixtures extends Fixture
{
    public const CATEGORY_REFERENCE = 'category';

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();


        for ($i=0; $i < 4; $i++) { 
            
            $category = new Category();
            $category->setName($faker->name());
            $manager->persist($category);
            $this->addReference(self::CATEGORY_REFERENCE.$i, $category);
        }

        $manager->flush();
    }
    
}

