## Ecommerce Back Laure Yann Chems Samantha

##Organisation

- Mise en place d'un git et d'un board avec des issues,
- Mise en place de Fixtures avec des système de boucles et de références selon notre diagrame de classe afin de remplir notre base de données,
- Mise au point et réunions à chaque début de session,
- Tous le monde a créé au moins une partie de chaque élément, afin de tous monter en compétence,
- Livecoding et explications afin de mettre à niveau et au courant tous le monde,
- Utilisation de doctrine pour créer les entités, leur repository et leurs relations,
- Création des controlleurs, de méthodes personnalisées et des routes nécessaires,
- Tests de nos routes grâce au ThunderClient et à notre jeu de données,
- Ajout d'Ignores sur certains éléments pour éviter les circular réference,
- Mise en place de l'authentification du user avec un système de token JWT,
- Création d'accès limités sur certaines routes,
- Création d'un service Uploader pour l'upload d'image avec un dossier uploads dans public,


##Documents:

Nos [Users stories, à voir ici.](https://docs.google.com/document/d/1nX7HICHSvX4IIngABOUL1_abs-Os2Z_kvlV_Fn_PzX0/edit?usp=sharing)

User Case: ![<alt>](</public/img/UseCaseDiagram1.jpg>)

Diagramme de classe: ![<alt>](</public/img/Diagramme_de_classe.png>)

Les Wireframes ont été réalisés avec [Figma, à voir ici.](https://www.figma.com/file/DUZ6Fjeh8Tq0O5HJuaa5hd/Ecommerce-Laure%2FChems%2FYann%2Fsam?node-id=0%3A1&t=mQGEtih8stPN8xpf-1)

## Les outils utilisés

- StarUML pour les diagrammes de classes et les userCases, 
- Figma pour les wireframes, 
- MariaDB pour la base de donnée MySQL, 
- Symfony pour le back avec Doctrine,
- React pour le front,
- Gitlab.


## Améliorations envisagées

- Ajout de méthodes personnalisées ou modifications de certains controlleurs en lien avec les fonctionnalités du site (par exemple la gestion du stock d'un produit lors de la validation d'une commande, l'ajout d'une condition si une lineproduct existe déjà dans un panier pour incrémenter celle-ci et non en créer une nouvelle...)

- Ajout de vraies données dans notre base de données pour remplacer les fixtures.

- Ajout de tests en back,

